# Copyright © 2019 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

glproc = custom_target(
  'glproc',
  input : 'glproc.py',
  output : ['glproc.hpp', 'glproc.cpp'],
  depend_files : [
    'dispatch.py',
    '../specs/wglapi.py',
    '../specs/glxapi.py',
    '../specs/cglapi.py',
    '../specs/glapi.py',
    '../specs/stdapi.py',
    '../specs/gltypes.py',
  ],
  command : [prog_py, '@INPUT@', '@OUTPUT@']
)

libglproc = static_library(
  'glproc',
  glproc,
  gnu_symbol_visibility : 'hidden',
  include_directories : inc_common,
  dependencies : dep_khr,
)

idep_glproc = declare_dependency(
  link_with : libglproc,
  sources : glproc[0],
)

libglproc_gl = static_library(
  'glproc_gl',
  'glproc_gl.cpp',
  include_directories : inc_common,
  gnu_symbol_visibility : 'hidden',
  dependencies : [idep_glproc, dep_khr],
)

if host_machine.system() != 'windows'
  libglproc_egl = []
  if get_option('egl')
    libglproc_egl = static_library(
      'glproc_egl',
      'glproc_egl.cpp',
      include_directories : inc_common,
      gnu_symbol_visibility : 'hidden',
      dependencies : [idep_glproc, dep_khr],
    )
  endif
endif

inc_dispatch = include_directories('.')

dep_glproc_gl = declare_dependency(
  link_with : [libglproc_gl],
  dependencies : [idep_glproc],
  include_directories : include_directories('.'),
)
